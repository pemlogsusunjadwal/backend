# Backend for Pemlog Assingment
###
Repository ini digunakan sebagai API endpoint dari repository frontend.
API ini menyediakan endpoint untuk mendapatkan semua mata kuliah yang ada di
https://academic.ui.ac.id/main/Authentication/
# Instalasi
###
- clone repository ini dengan cara
  ```
  git clone https://gitlab.com/pemlogsusunjadwal/backend.git
  ```
- masuk ke folder backend, nama folder dapat bebas diganti setelah di clone
  ```
  cd backend
  ```
- inisiasi git di folder yang kamu buat
  ```
  git init
  ```
- buat virtual environment
  ```
  python -m venv env
  ```
  atau
  ```
  python -m virtualenv env
  ```
- aktifkan virtual environment
  - untuk windows
    ```
    env/Scripts/activate.bat
    ```
  - untuk linux/mac
    ```
    source env/bin/activate
    ```
- install semua requirement yang ada, pastikan kamu berada di folder yang sama dengan file requirements.txt
  ```
  pip install -r requirements.txt
  ```
- cek apakah sudah bisa runserver
  ```
  python manage.py runserver
  ```
- buat file .env di backend/spidey/
  - isi .env
  ```
  SIAK_USERNAME="username akun siak anda"
  SIAK_PASSWORD="password akun siak anda"


# Endpoint
### Asumsi base_url = localhost:8000
  - (GET)parse course result from siak
    ```/api/schedule/courses/{semester aktif}/{kode fakultas}```
