import os

import jwt
from datetime import timedelta
from functools import reduce

from django.db.models.signals import post_save
from django.dispatch import receiver

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext as _
from django.utils import timezone

# from .utils import queue_send_activation_email


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_staff", True)

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")

        return self._create_user(email, password, **extra_fields)


class Account(AbstractBaseUser, PermissionsMixin):
    """
    Represents account and authentication model
    """

    objects = UserManager()
    # required fields
    name = models.CharField(max_length=64)
    email = models.EmailField(unique=True)
    verified_email = models.EmailField(unique=True, null=True, blank=True)

    # some metas
    is_staff = models.BooleanField(_("staff"), default=False)
    is_superuser = models.BooleanField(_("superuser"), default=False)
    is_active = models.BooleanField(_("active"), default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # external authentications
    ui_sso_username = models.CharField(
        _("SSO UI username"), max_length=64, blank=True, null=True, unique=True
    )
    # UI lecturers and staff might not have this field, but it's nice to have
    # this field for later analysis
    ui_sso_npm = models.CharField(
        _("SSO UI NPM"), max_length=16, blank=True, null=True)
    ui_sso_fac_code = models.CharField(
        _("SSO UI Org. Code"), max_length=16, blank=True, null=True)
    ui_sso_study_program = models.CharField(
        _("SSO UI Study Program"), max_length=100, blank=True, null=True)
    ui_sso_educational_program = models.CharField(
        _("SSO UI Educational Program"), max_length=100, blank=True, null=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    @property
    def usable_email(self):
        if self.verified_email:
            return self.verified_email
        return self.email

    @property
    def is_email_verified(self):
        return self.verified_email == self.email

    @property
    def is_paired_sso(self):
        """
        returns true if user has paired this account with SSO UI
        """
        return self.ui_sso_username is not None

    @property
    def signed_id(self):
        return jwt.encode({"id": self.id, "name": self.name}, settings.JWT_SECRET_KEY)

    def __str__(self):
        return f"{self.name} <{self.usable_email}>"

