from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth.password_validation import (
    CommonPasswordValidator,
    MinimumLengthValidator,
    NumericPasswordValidator,
    UserAttributeSimilarityValidator,
)
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.compat import authenticate

from account.models import Account

class AccountSerializer(serializers.ModelSerializer):
    def validate(self, data):
        if "email" in data.keys():
            data["email"] = data["email"].lower()
        return data

    class Meta:
        model = Account
        fields = (
            "id",
            "name",
            "email",
            "is_staff",
            "is_active",
            "is_paired_sso",
            "is_email_verified",
            "has_usable_password",
            "usable_email",
            "ui_sso_fac_code",
            "ui_sso_study_program"
        )
        read_only_fields = (
            "is_active",
            "is_staff",
            "is_superuser",
            "is_paired_sso",
            "has_usable_password",
            "usable_email",
            "is_email_verified",
            "ui_sso_fac_code",
            "ui_sso_study_program"
        )