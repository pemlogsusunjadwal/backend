import json
from django.http import HttpResponse
from django.template import Context, Template


def clean_username(username):
    return username.lower()


def post_message_renderer(sender, error=None, body=None):
    context = {"sender": sender, "payload": json.dumps(
        {"error": error, "body": body})}

    return HttpResponse(
        Template(
            """
    <!DOCTYPE html>
        <html><head><meta charset="UTF-8"><title>Please Wait</title></head>
        <body>Please Wait...</body>
        <script type="application/javascript">
            if (window.opener) {
                console.log({{ payload|safe }});
                console.log("{{sender|safe}}");
                window.opener.postMessage({{ payload|safe }}, "{{sender|safe}}");
            } else {
                var url_string = window.location.href;
                var url = new URL(url_string);
                var next = url.searchParams.get("next");

                var hostname = window.location.hostname;
                var protocol = window.location.protocol;
                var port = window.location.port;
                if (port.length > 0) {
                    port = ':' + port;
                }

                console.log(protocol + '://' + hostname + port + next);
                if (next !== '/') {
                    window.location = protocol + '//' + hostname + port + next;
                }
            }
        </script>
    </html>
    """
        ).render(Context(context)),
        status=400 if error else 200,
    )
