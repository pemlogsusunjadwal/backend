import json
import string
from urllib.parse import parse_qs, quote, unquote

from django.contrib.auth import login
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.conf import settings
from django.urls import reverse

from django_sso_ui.decorators import with_sso_ui
from account.utils import clean_username, post_message_renderer
from account.token import payload_handler, get_token
from account.models import Account

DOMAIN = "ui.ac.id"
SELF = settings.API_DOMAIN
# Create your views here.

@with_sso_ui(force_login=True)
def auth(request, sso_profile):
    action = request.GET.get(
            "action") or request.session.get("action", "login")
    sender = request.GET.get(
        "sender") or request.session.get("sender", SELF)

    username = clean_username(sso_profile["username"])
    # TODO:
    # solve hardcode in sender variable
    # affect the parameter in post_message_renderer
    if action == "login":
        if request.user.is_authenticated:
            if request.user.ui_sso_username != username:
                return post_message_renderer(settings.FRONTEND_DOMAIN, "Unauthorized")

        email = username + "@" + DOMAIN
        account = Account.objects.get_or_create(email=email, defaults={
            "verified_email": email,
            "name": string.capwords(sso_profile["attributes"]["nama"]),
            "ui_sso_username": username,
            "ui_sso_npm": sso_profile["attributes"].get("npm", None),
            "ui_sso_fac_code": sso_profile["attributes"].get("kd_org", None),
            "ui_sso_study_program": sso_profile["attributes"].get("study_program"),
            "ui_sso_educational_program": sso_profile["attributes"].get("educatioanl_program")
        })[0]

        if sender == SELF:
            login(request, account)
        return post_message_renderer(settings.FRONTEND_DOMAIN, body=payload_handler(get_token(account), account))