from django.urls import path,include
from account.views import auth


app_name = "account"

urlpatterns = [
    path("auth/", auth, name="auth")
]