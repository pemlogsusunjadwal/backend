from haikunator import Haikunator
from rest_framework.authtoken.models import Token

from account.serializers import AccountSerializer


def payload_handler(token, user):
    return {"token": token, "user": AccountSerializer(user).data}


def get_token(user):
    token, _ = Token.objects.get_or_create(user=user)
    return token.key


def refresh_token(user):
    if Token.objects.filter(user=user).exists():
        Token.objects.get(user=user).delete()


def get_haikunator_token():
    return Haikunator().haikunate(token_length=6, token_hex=True)
