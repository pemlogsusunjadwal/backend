from django.urls import path,include
from .views import GetSchedule


app_name = "schedule"

urlpatterns = [
    path("courses/<active_period>/<kd_org>/", GetSchedule.as_view(), name="get_schedule")
]