from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .parser import scrape_courses
from .models import Period
from .serializers import PeriodSerializer
import json

class GetSchedule(APIView):

    # permission_classes = (IsAuthenticated,)
    lookup_field = ("kd_org", "active_period")

    def get(self, request, kd_org, active_period):
        try:
            period = Period.objects.filter(major_id=kd_org, period=active_period)
            if len(period) > 0:
                return Response({
                    "courses": json.loads(period[0].schedule),
                    "period": period[0].period
                },
                status=status.HTTP_200_OK
                )
            new_period = Period.objects.create(
                period=active_period,
                major_id=kd_org,
                schedule=json.dumps(scrape_courses(kd_org, active_period, True))
            )
            new_period.save()
            return Response({
                "courses": json.loads(new_period.schedule),
                "period": new_period.period
            },
            status=status.HTTP_200_OK
            )
        except Exception as e:
            print(str(e))
            return Response(
                {
                    "data": str(e)
                },
                status=status.HTTP_400_BAD_REQUEST
            )
