from django.db import models

class Period(models.Model):

    period = models.CharField(max_length=10)
    major_id = models.CharField(max_length=16)
    schedule = models.TextField()

    def __str__(self):
        return self.period
