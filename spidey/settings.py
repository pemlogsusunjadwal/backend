"""
Django settings for spidey project.

Generated by 'django-admin startproject' using Django 2.2.6.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
import socket
import dj_database_url
import environ



# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PRODUCTION = os.environ.get('DATABASE_URL') != None

root = environ.Path(BASE_DIR)   # get root of the project
env = environ.Env()
environ.Env.read_env()  # reading .env file

SIAK_USERNAME = env('SIAK_USERNAME')
SIAK_PASSWORD = env('SIAK_PASSWORD')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'peph@augv6^64og=$tt6i6-jrec2@*dhk1kcvnw5x*(wbyxh#@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = PRODUCTION


if not DEBUG:
    FRONTEND_DOMAIN = "http://localhost:3000"
    API_DOMAIN = "http://localhost:8000"
else:
    FRONTEND_DOMAIN = "https://susun-jadwal-ng.netlify.app"
    API_DOMAIN = "api-susunjadwal-ng.herokuapp.com"


# Application definition

APPS = [
    "account",
    "schedule"
]

MODULES = [
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    "django_cas_ng",
]

INSTALLED_APPS = (
    [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
    ]
    +APPS
    +MODULES
)



ALLOWED_HOSTS = ["localhost", "api-susunjadwal-ng.herokuapp.com"]
SITE_ID = 1
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = (
    'localhost:3000',
    'http://localhost:3000',
    'localhost',
    'https://susun-jadwal-ng.netlify.app'
)

SSO_UI_URL="https://sso.ui.ac.id/cas2/"
SSO_UI_FORCE_SERVICE_HTTPS=False
DATA_UPLOAD_MAX_NUMBER_FIELDS = 10240

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    "corsheaders.middleware.CorsMiddleware",
]

AUTH_USER_MODEL = "account.Account"

AUTHENTICATION_BACKENDS = ("django.contrib.auth.backends.ModelBackend",)

ROOT_URLCONF = 'spidey.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'spidey.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

if PRODUCTION:
    DATABASES['default'] = dj_database_url.config()


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')
# Extra places for collectstatic to find static files.
# STATICFILES_DIRS = [
#     os.path.join(BASE_DIR, 'static'),
# ]

CREDENTIALS_SIAK = {
    "09.00.12.01":{
        "username": SIAK_USERNAME,
        "password": SIAK_PASSWORD
    },
    "01.00.12.01":{
        "username": SIAK_USERNAME,
        "password": SIAK_PASSWORD
    },
    "06.00.12.01":{
        "username": SIAK_USERNAME,
        "password": SIAK_PASSWORD
    },
    "02.00.12.01":{
        "username": SIAK_USERNAME,
        "password": SIAK_PASSWORD
    },
    "08.00.12.01":{
        "username": SIAK_USERNAME,
        "password": SIAK_PASSWORD
    }
}
